# -*- coding: utf-8 -*-
def geraMatriz(n=10, valor_padrao=0):
    matriz = []
    for i in range(n):
        matriz.append([])
        for j in range(n):
            matriz[i].append(valor_padrao)
    return matriz
    
def imprimeMatriz(mat):
    for m in mat:
        print(m)
    
def insereAresta(mat, i, j, peso=1):
    mat[i][j] = peso
    mat[j][i] = peso

def verticeAdj(mat, vertice):
    adj = []
    for i in range(len(mat)):
        if(mat[vertice][i] == 1):
            adj.append(i)
    return adj

def existeAresta(mat, v1, v2):
    if(mat[v1][v2] == 1 or mat[v2][v1] == 1):
        return True
    else:
        return False
# Gerar matriz
g = geraMatriz(3)

# Imprimir matriz
# imprimeMatriz(g)
# Inserir aresta
insereAresta(g,0,1)
insereAresta(g,1,2)
insereAresta(g,2,0)
imprimeMatriz(g)
print ('Vertices adjacentes 1:', verticeAdj(g,1))
print ('Existe aresta entre 0 e 1: ',existeAresta(g,0,1))
print('Grau do vertice 1: ', len(verticeAdj(g,1)))