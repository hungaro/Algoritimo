import os

grafo = {'a': [('p',4), ('j',15), ('b',1)],
         	'b': [('a',1), ('d',2), ('e',2), ('c',2)],
			'j': [('a',15),('c',6)],
			'p': [('a',4),('d',8)],
			'd': [('b',2), ('g',3),('p',8)],
			'e': [('b',2), ('g',9), ('f',5), ('c',2),('h',4)],
			'c': [('b',2), ('e',2), ('f',5), ('i',20),('j',6)],
			'g': [('d',3), ('e',9), ('h',1)],
			'f': [('h',10), ('e',5), ('c',5),('i',2)],
			'i': [('c',20),('f',2)],
			'h': [('g',1),('e',4),('f',10)] 
		}

listaVisitados = []
grafoResultante = {}
listaOrdenada = []


origen = input("\nColoque o no de origem: ")

listaVisitados.append(origen)

for destino, peso in grafo[origen]:
  listaOrdenada.append((origen, destino, peso))

pos=0
act=0
listAux=[]
for i in range(len(listaOrdenada)):
    listAux=listaOrdenada[i]
    act=listaOrdenada[i][2]
    pos=i
    while pos> 0 and listaOrdenada[pos-1][2] > act:
        listaOrdenada[pos] = listaOrdenada[pos-1]
        pos=pos-1
    listaOrdenada[pos]=listAux

while listaOrdenada:

  vertice = listaOrdenada.pop(0)
  d = vertice[1]

  if d not in listaVisitados:

    listaVisitados.append(d)

    for key, lista in grafo[d]:
      if key not in listaVisitados:
        listaOrdenada.append((d, key, lista))

    listaOrdenada = [(c,a,b) for a,b,c in listaOrdenada]
    listaOrdenada.sort()
    listaOrdenada = [(a,b,c) for c,a,b in listaOrdenada]

    origen  = vertice[0]
    destino = vertice[1]
    peso    = vertice[2]

    if origen in grafoResultante:
      if destino in grafoResultante:
        lista = grafoResultante[origen]
        grafoResultante[origen] = lista + [(destino, peso)]
        lista = grafoResultante[destino]
        lista.append((origen, peso))
        grafoResultante[destino] = lista
      else:
        grafoResultante[destino] = [(origen, peso)]
        lista = grafoResultante[origen]
        lista.append((destino, peso))
        grafoResultante[origen] = lista
    elif destino in grafoResultante:
      grafoResultante[origen] = [(destino, peso)]
      lista = grafoResultante [destino]
      lista.append((origen, peso))
      grafoResultante[destino] = lista
    else:
      grafoResultante[destino] = [(origen, peso)]
      grafoResultante[origen] = [(destino, peso)]
      
print("\n\nGrafo resultante:\n")
for key, lista in grafoResultante.items():
  print(key)
  print(lista)